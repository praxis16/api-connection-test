package com.praxis.apitest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button bt = (Button) findViewById(R.id.bt);
        final EditText url = (EditText) findViewById(R.id.url);
        final EditText method = (EditText) findViewById(R.id.method);
        final EditText body = (EditText) findViewById(R.id.body);
        final TextView op = (TextView) findViewById(R.id.op);
        bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                JSONObject req = new JSONObject();
                if(url.getText().toString().equals("")) {
                    Toast.makeText(MainActivity.this, "Add url", Toast.LENGTH_SHORT).show();
                } else if(!method.getText().toString().equals("GET") && !method.getText().toString().equals("GET") && !method.getText().toString().equals("POST") && !method.getText().toString().equals("PUT") && !method.getText().toString().equals("DELETE")){
                    Toast.makeText(MainActivity.this, "Add proper method", Toast.LENGTH_SHORT).show();
                } else if((method.getText().toString().equals("POST") || method.getText().toString().equals("PUT")) && body.getText().toString().equals("")) {
                    Toast.makeText(MainActivity.this, "Add JSON body", Toast.LENGTH_SHORT).show();
                } else {
                    try {
                        if(!body.getText().toString().equals(""))
                            req.put("body", new JSONObject(body.getText().toString()));
                        req.put("method", method.getText().toString());
                        req.put("url", url.getText().toString());
                        new APIMiddleware(new APIMiddleware.TaskListener() {
                            @Override
                            public void onTaskBegin() {
                                op.setText("begun");
                            }

                            @Override
                            public void onTaskCompleted(JSONObject response) {
                                if(response != null) {
                                    op.setText(response.toString());
                                } else {
                                    op.setText("done");
                                }
                            }
                        }).execute(req);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }
        });
    }
}
